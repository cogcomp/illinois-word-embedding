package edu.illinois.cs.cogcomp.nlp.wordmap;

import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class WordMapTest {


    @Test
    public void utilityTest() {
        String wordfile = "src/test/resources/mini-word-embedding.txt";
        EfficientWordMap vectors = null;
        boolean useCache = false;
        
        try {
            vectors = new EfficientWordMap(wordfile,300,useCache);
        }
        catch(FileNotFoundException e) {
        }
        
        assertEquals(vectors.cosineSim("dog", "hound"),0.803,0.001);
    }
    
    @Test
    public void unknownTestWithVector() {
        String wordfile = "src/test/resources/mini-word-embedding.txt";
        EfficientWordMap vectors = null;
        boolean useCache = false;
        
        try {
            vectors = new EfficientWordMap(wordfile,300,useCache);
        }
        catch(FileNotFoundException e) {
        }
        
        assertEquals(vectors.cosineSim("dog", "unknown"),0.026,0.001);
        assertEquals(vectors.cosineSim("unknown", "unknown"),1.000,0.001);
    }
    
    @Test
    public void unknownTestWithoutVector() {
        String wordfile = "src/test/resources/mini-word-embedding-no-unk.txt";
        EfficientWordMap vectors = null;
        boolean useCache = false;
        
        try {
            vectors = new EfficientWordMap(wordfile,300,useCache);
        }
        catch(FileNotFoundException e) {
        }
        
        assertEquals(vectors.cosineSim("dog", "unknown"),0.000,0.001);
        assertEquals(vectors.cosineSim("unknown", "unknown"),1.000,0.001);
    }
}
